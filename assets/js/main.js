(function ($) {
	"use strict";

    jQuery(document).ready(function($){


        $(".embed-responsive iframe").addClass("embed-responsive-item");
        $(".carousel-inner .item:first-child").addClass("active");
        
        $('[data-toggle="tooltip"]').tooltip();


        $("a.menu-trigger").on("click", function(){
            $(".off-canvas-menu, .off-canvas-overlay").addClass("active");
            return false;
        });
        
        $(".menu-close, .off-canvas-overlay").on("click", function(){
            $(".off-canvas-menu, .off-canvas-overlay").removeClass("active");
        });
        
        
        $(".menu-close, .off-canvas-overlay").on("click", function(){
            $(".off-canvas-overlay").removeClass("active");
        });
        
        $(".owl-carousel").owlCarousel({
            items: 1,
            nav: true,
            dots: false,
            autoplay: false,
            loop: true,
            navText: ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
            mouseDrag: true,
            touchDrag: false,
            animateIn: 'fadeIn', // add this
            animateOut: 'fadeOut' // and this
        });

        //SMOOTH SCROLL JS
        $('a.smoth-scroll').on("click", function (e) {
            var anchor = $(this);
            $('html, body').stop().animate({
                scrollTop: $(anchor.attr('href')).offset().top - 15
            }, 1000);
            e.preventDefault();
        });

         //SMOOTH SCROLL JS
        $('a.smoth-scroll.cause-smoth').on("click", function (e) {
            var anchor = $(this);
            $('html, body').stop().animate({
                scrollTop: $(anchor.attr('href')).offset().top - 90
            }, 1000);
            
            e.preventDefault();
        });

        


        var stickyHeader = function(){
 
          //THE POSITION OF TOP OF PAGE IN BROWSER WINDOW
          var windowTopPosition = $(window).scrollTop();
          
          //THE POSITION OF ARTICLE TOP
          var triggerContainer = $('.banner-area').offset().top;

          //THE THING THATS STICKS
          var stickyContainer = $('.header-area');
          
          if (windowTopPosition > triggerContainer) {    
            stickyContainer.addClass('sticky');
            stickyContainer.slideDown('fast');
            
          }else{
            stickyContainer.removeClass('sticky');
            stickyContainer.css("display","");
          }
          
          
        };

        $(window).scroll(stickyHeader);


    });


    jQuery(window).load(function(){

        
    });


}(jQuery));	